var editor = ace.edit("editor");
var editorSession = editor.getSession();

editor.setOptions({
    fontSize: "13px",
    highlightActiveLine: false
});

editor.container.style.lineHeight = 1.6;

editor.setTheme("ace/theme/chrome");
editor.setShowPrintMargin(false);

editor.renderer.setHighlightGutterLine(false);

editorSession.setMode("ace/mode/sh");
editor.$blockScrolling = Infinity;

// Textarea handler
var textArea = $('textarea[name="raw"]').hide();
editorSession.setValue(textArea.val());
editorSession.on('change', function () {
    textArea.val(editorSession.getValue());
});

// Jump to the last line
var row = editorSession.getLength() - 1;
var column = editorSession.getLine(row).length;
editor.gotoLine(row + 1, column);