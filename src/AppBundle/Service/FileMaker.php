<?php


namespace AppBundle\Service;

use Symfony\Component\Filesystem\Filesystem;

class FileMaker {

	/** @var Filesystem */
	protected $filesystem;

	/** @var string */
	protected $targetDir;

	public function __construct( Filesystem $filesystem, string $targetDir )
	{
		$this->filesystem = $filesystem;
		$this->targetDir  = $targetDir;
	}

	public function make( string $content, string $filename = null, string $extension = 'txt' ): string
	{
		$filenamePrefix = uniqid( 'raw_', false );

		if (! $filename) {
			$filename = sprintf( '%s.%s', $filenamePrefix, $extension );
		}

		$filePath = sprintf( '%s/%s', $this->targetDir, $filename );

		$this->filesystem->dumpFile($filePath, $content);

		return $filename;
	}

	/**
	 * @return string
	 */
	public function getTargetDir(): string
	{
		return $this->targetDir;
	}

	/**
	 * @return Filesystem
	 */
	public function getFilesystem(): Filesystem
	{
		return $this->filesystem;
	}
}