<?php


namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
	/** @var string */
	private $targetDir;

	/**
	 * FileUploader constructor.
	 *
	 * @param string $targetDir
	 */
	public function __construct(string $targetDir)
	{
		$this->targetDir = $targetDir;
	}

	/**
	 * @param UploadedFile $file
	 *
	 * @return string
	 *
	 * @throws FileException
	 */
	public function upload(UploadedFile $file): string
	{
		$fileName = md5(uniqid('img', false)).'.'.$file->guessExtension();

		$file->move($this->getTargetDir(), $fileName);

		return $fileName;
	}

	/**
	 * @return string
	 */
	public function getTargetDir(): string
	{
		return $this->targetDir;
	}
}