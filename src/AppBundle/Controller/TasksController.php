<?php


namespace AppBundle\Controller;


use AppBundle\Entity\Task;
use AppBundle\Form\TaskType;
use AppBundle\Service\FileMaker;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TasksController
 * @package AppBundle\Controller
 *
 * @Route("/tasks")
 */
class TasksController extends Controller {

	public function indexAction()
	{
		$tasks = $this->getDoctrine()->getRepository( 'AppBundle:Task' )->findAll();

		return $this->render( '', compact( 'tasks' ) );
	}

	public function showAction( Task $task )
	{
		return $this->render( '', compact( 'task' ) );
	}

	/**
	 * @param Request   $request
	 * @param FileMaker $fileMaker
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 *
	 * @Route("/create", name="tasks_create")
	 */
	public function createAction( Request $request, FileMaker $fileMaker )
	{
		$form = $this->createForm( TaskType::class, $task = new Task() );
		$form->handleRequest( $request );

		if ( $form->isSubmitted() && $form->isValid() ) {
			$raw = $request->request->get( 'raw' );

			$filename = $fileMaker->make($raw);

			$task->setFilename($filename);

			$this->getManager()->persist($task);
			$this->getManager()->flush();

			return $this->redirectToRoute('tasks_create');
		}

		return $this->render( ':Task:create.html.twig' );
	}

	public function updateAction()
	{
	}

	public function deleteAction()
	{
	}

	/**
	 * @return \Doctrine\Common\Persistence\ObjectManager|object
	 */
	private function getManager(): ObjectManager
	{
		return $this->getDoctrine()->getManager();
	}
}